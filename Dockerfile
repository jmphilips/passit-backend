FROM python:3.6-alpine
ENV PYTHONUNBUFFERED 1
ENV PORT 8080

RUN mkdir /code
WORKDIR /code

# gcc & libffi-dev -> from cffi>=1.7 (from cryptography>=1.9->pyOpenSSL->django-rest-knox==3.0.0
# musl-dev & linux-headers -> psycopg2 -> https://stackoverflow.com/questions/30624829/no-such-file-or-directory-limits-h-when-installing-pillow-on-alpine-linux
RUN apk add --update --no-cache \
  libsodium-dev \
  postgresql-dev \
  libffi-dev \
  gcc \
  musl-dev \
  linux-headers

COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

EXPOSE 8080

COPY . /code/

CMD ["./bin/start.sh"]
