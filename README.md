# Passit Backend

This is the Django backend for passit.io. It's duty is to:

- Store user (encrypted) data
- Have no way to read the data ever - not even storing critital data in ram
- Authenticate users and ensure they can only access what they made or have access to.
- Run tests to verify the general security model

# Development

1. `docker-compose up`
2. Run migrations `docker-compose run --rm web ./manage.py migrate`
3. (optional) load fixtures `docker-compose run --rm web ./manage.py loaddata users groups`

Go to localhost:8000/api to view the api.

### Run dev with celery

Just add the worker compose file like `docker-compose -f docker-compose.yml -f docker-compose.worker.yml up`

## Testing

`docker-compouse run --rm web ./manage.py test`

## Contributing 

You may submit issues to this repo directly or to the passit meta project [here](https://gitlab.com/passit/passit).

# Settings

All settings can be managed with environment variables.

# SSL

Passit is not as secure when run without SSL.

- `IS_DEBUG` - Defaults to False. Leave this as default to be secure.
- `SECURE_SSL_REDIRECT` Defaults to `False`. Set this to True if you'd like Django to redirect HTTP to HTTPS for you. This may not work with all load balancers.
- `SECURE_COOKIE` - Defaults to True. Only set to False is not using SSL

# Celery

Celery is optional. It is recommended only for large scale deployments like app.passit.io
Celery can run tasks like sending email or recording analytics.

- `CELERY_USE_FOR_EMAIL` - Send email using celery. Defaults to False.
- `CELERY_BROKER_URL` - Celery broker (like redis). Example: `redis://redis:6379/0`

## Piwik

Piwik requires running celery. Once celery is configured, set the following env vars:

- `PIWIK_SITE_ID` - Your site's piwik ID. Example: `1`
- `PIWIK_API_URL` - Where you piwik server is hosted. Example: `https://your.site.com/piwik.php`
- `PIWIK_TOKEN_AUTH` - Your piwik API auth (settings, api) Example: `0000000000`

# Logging and error reporting

- `DJANGO_LOG_LEVEL` - Defaults to INFO. Valid settings are 'DEBUG', 'INFO', 'WARNING', 'ERROR', and 'CRITICAL'.

Sentry/Raven error handling support is optional and is really only needed for testing and large scale deployments. To enable set the following:

- `RAVEN_DSN` - Set this to enable Sentry error reporting.
- `ENVIRONMENT` - Note which environment passit is running in (Staging, Production, etc). At this time only sentry makes use of this.
