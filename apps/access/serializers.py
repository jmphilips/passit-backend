from django.conf import settings
from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from .models import User, Group, GroupUser
from .utils import is_acceptable_email
import warnings


class UserPublicAuthSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('client_salt',)


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = (
            'id', 'email', 'password', 'public_key', 'private_key',
            'client_salt')
        read_only_fields = ('id',)
        extra_kwargs = {
            'password': {'write_only': True}
        }

    def validate_email(self, value):
        if not is_acceptable_email(value):
            raise serializers.ValidationError("This email is not allowed.")
        return value

    def create(self, validated_data):
        return User.objects.create_user(**validated_data)


class PasswordSerializer(serializers.Serializer):
    password = serializers.CharField()


class ContactSerializer(serializers.ModelSerializer):
    """ Right now this returns just users, it will change in the future """
    class Meta:
        model = User
        fields = ("id", "email", "first_name", "last_name")


class PublicUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'public_key',)


class LookupUserIdSerializer(serializers.Serializer):
    email = serializers.EmailField()


class UserIdSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id',)


class UserPublicKeySerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'public_key',)


class UsernameAvailableSerializer(serializers.Serializer):
    email = serializers.EmailField(max_length=254)

    def validate_email(self, value):
        if not is_acceptable_email(value):
            raise serializers.ValidationError("This email is not allowed.")
        return value


class GroupSerializer(serializers.ModelSerializer):
    key_ciphertext = serializers.CharField(write_only=True)
    private_key_ciphertext = serializers.CharField(write_only=True)

    class Meta:
        model = Group
        fields = ('id', 'name', 'slug', 'public_key', 'key_ciphertext',
                  'private_key_ciphertext')
        extra_kwargs = {
            'public_key': {'write_only': True},
        }

    def create(self, validated_data):
        user = self.context['request'].user
        return Group.objects.create(user, **validated_data)


class GroupUserSerializer(serializers.ModelSerializer):
    group = serializers.PrimaryKeyRelatedField(
        required=False, allow_null=True, read_only=True, default=None)
    is_invite_pending = serializers.SerializerMethodField()
    # Needed only to confirm someone adding a user to a group knows the email
    # Prevents adding users by guessing ids (which are sequential)
    user_email = serializers.EmailField(write_only=True, required=False)

    class Meta:
        model = GroupUser
        fields = ('id', 'user', 'user_email', 'group', 'is_group_admin',
            'key_ciphertext', 'private_key_ciphertext', 'is_invite_pending')
        read_only_fields = ('group', 'is_invite_pending')
        extra_kwargs = {
            'key_ciphertext': {'write_only': True},
            'private_key_ciphertext': {'write_only': True},
            'user': {'html_cutoff': 0},
        }

    def get_is_invite_pending(self, obj) -> bool:
        return obj.groupuserinvite_set.exists()

    def validate(self, data):
        if GroupUser.objects.filter(
            user=data.get('user'),
            group=self.context['view'].get_group()
        ).exists():
            raise serializers.ValidationError(
                "A user can only be added to a group once. ")
        return super().validate(data)

    def create(self, validated_data):
        """ When not in private org mode - any add to group is always a invite
        """
        if (validated_data.pop('user_email', None) != validated_data['user'].email):
            raise ValidationError("user_email must match user's email")
        groupuser = GroupUser.objects.create(**validated_data)
        if not settings.IS_PRIVATE_ORG_MODE:
            inviter = self.context['request'].user
            groupuser.groupuserinvite_set.create(inviter=inviter)
        return groupuser


class GroupUserUpdateSerializer(serializers.ModelSerializer):
    """ No need to change the group or user when updating a through table """
    class Meta:
        model = GroupUser
        fields = ('id', 'user', 'group', 'is_group_admin',)
        read_only_fields = ('user', 'group',)


class DetailedGroupSerializer(serializers.ModelSerializer):
    groupuser_set = GroupUserSerializer(many=True)
    my_key_ciphertext = serializers.SerializerMethodField()
    my_private_key_ciphertext = serializers.SerializerMethodField()

    def get_my_key_ciphertext(self, obj):
        user = self.context['request'].user
        # Optimization - searching this way uses prefetch_related
        for groupuser in obj.groupuser_set.all():
            if groupuser.user_id == user.id:
                return groupuser.key_ciphertext

    def get_my_private_key_ciphertext(self, obj):
        user = self.context['request'].user
        # Optimization - searching this way uses prefetch_related
        for groupuser in obj.groupuser_set.all():
            if groupuser.user_id == user.id:
                return groupuser.private_key_ciphertext

    class Meta:
        model = Group
        fields = ('id', 'name', 'groupuser_set', 'public_key',
                  'my_key_ciphertext', 'my_private_key_ciphertext')

class AcceptRejectGroupUserSerializer(serializers.Serializer):
    id = serializers.IntegerField()
