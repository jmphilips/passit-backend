from django.core.urlresolvers import reverse
from django.test import override_settings
from rest_framework.test import APITestCase, APIClient
from model_mommy import mommy
from passit.test_client import PassitTestMixin
from passit_sdk import PassitTestClient
from passit_sdk.exceptions import (
    PassitAPIException, PassitAuthenticationRequired, PassitSDKException)
from .models import User, Group, GroupUser, GroupUserInvite
from ..secrets.models import Secret, SecretThrough
from constance.test import override_config


TEST_USER_USERNAME = 'noadmin@aa.aa'
TEST_USER_PASSWORD = 'bbbbbbbb'
FAKE_CIPHERTEXT = 'a' * 96


class TestAuthAPI(PassitTestMixin, APITestCase):
    fixtures = ['users.json']

    def test_ping(self):
        """ Clients use this to check if server exists """
        url = reverse('ping')
        res = self.client.get(url)
        self.assertEqual(res.status_code, 200)
        self.assertEqual(res.data['ping'], 'pong')

    def test_username_available(self):
        result = self.sdk.is_username_available('aa@aa.aa')
        self.assertFalse(result)

        try:
            result = self.sdk.is_username_available('a' * 90000 + '@woo.com')
        except Exception as e:
            self.assertEqual(e.res.status_code, 400)

        result = self.sdk.is_username_available('jhgyujh@fdalfdsf.fdsa')
        self.assertTrue(result)

    @override_config(REGISTRATION_WHITELIST="*@example.com,me@example.org")
    def test_username_available_whitelist(self):
        should_pass = [
            'test@example.com',
            'testanother@example.com',
            'me@example.org',
        ]
        should_fail = [
            'notme@example.org',
            'anything@gmail.com',
            'almost@examplec.om',
        ]
        for item in should_pass:
            result = self.sdk.is_username_available(item)
            self.assertTrue(result)
        for item in should_fail:
            with self.assertRaises(PassitSDKException):
                self.sdk.is_username_available(item)

    def test_new_user(self):
        email = "testy@test.test"
        password = "12345678"

        self.sdk.sign_up(email, password)
        self.assertTrue(User.objects.get(email=email))

        with self.assertRaises(PassitAPIException):
            self.sdk.sign_up('nope', password)

    @override_config(REGISTRATION_WHITELIST="*@example.com")
    def test_new_user_invalid_email(self):
        email = "testy@test.test"
        password = "12345678"

        with self.assertRaises(PassitAPIException):
            self.sdk.sign_up(email, password)

    def test_new_user_fail(self):
        ''' Simulates an impossible state for the SDK to give, proves
        server side validation '''
        url = reverse('users-list')
        data = {
            "email": "testy@test.test",
            "password": "12345678",
            "private_key": "lol",
            "public_key": "not real",
        }
        res = self.client.post(url, data)
        self.assertEqual(res.status_code, 400)

        # Valid pub key but no private.
        data = {
            "email": "testy@test.test",
            "password": "12345678",
            "public_key": "MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEA5LKLLSzMNOhG01xc1gUr\nDSX19MKL+h0u0p9bFRZ+2KOrDG3LIQSGOYCXBVsZS9eeh3OwOHFcBi+3J55BL9rZ\n32l3E7sv75H6tOMHJzRQqAj8LtayEoDyq+k5FLeZliXt980rzwFIFZ0mARzg44xS\n+Rt3HziXp+COQiqKJob40hP2TfFDy2glU4uUCBVhTHLNogDc/dECrTp+76+2vYs0\nwDL86apjuUg4iQxN1u8cddOVA/I9HC1TkX3B0Iv1jwMRRZWB73BvvuxfksbtVV+X\nnyTIvZMrlep6bhupmN4gZacQsi8bWf19S7CdQ6AC6h9ghTg9G9Duci1mlPy6FleT\nAKNiXd1Vmk9mwRcTD81uXQPM9VsNqnNa3WoQHmDXrvALE/Pv++4LaujJEr3yYNXy\nsiHMSbTeh/0Gvt9CwGLgIoQpqhrMy4y5iIngfLQeKK6p6k785zpmRVgQyxtLrmiQ\nT9xz+cfAcC5Y6ZRs9i+SweC+tH4iN1v1DZMhDM1J8B/6Nj9pp64cb/tiYTw1JIyc\n2c2gW4ovZOyAZisVf9cAJWekwLpoxZdfNuQRwoU4Z1d7G+1bOtOHXJyJBGA3QmJc\nopp823HDGXqwDcBH/6lUic6yiyjCZJF7fyhgQxAuB5zHbBA1dFp2ZBMXoahEpTdB\nk22oA9jbRF3fIgCs8nqnSL0CAwEAAQ==\n-----END PUBLIC KEY-----",
        }
        res = self.client.post(url, data)
        self.assertEqual(res.status_code, 400)

    def test_delete_user(self):
        """ Ensure we can delete our own user, but not other users.
        Ensure all user secret through objects are destroyed as well.
        Group secret objects should still exist
        The user's personal (non group) secrets should be destroyed -
        both the Secret and SecretThrough.
        """
        self.sdk.log_in(TEST_USER_USERNAME, TEST_USER_PASSWORD)

        not_user = User.objects.exclude(id=self.sdk.user_id).first()
        url = reverse('users-detail', args=[not_user.id])
        # Should require password
        res = self.client.delete(url)
        self.assertEqual(res.status_code, 400)

        # should not accept wrong password
        data = {'password': 'wrong'}
        res = self.client.delete(url, data)
        self.assertEqual(res.status_code, 400)

        # Should not delete random user
        res = self.client.get(reverse('users-list'))
        client_salt = res.data[0]['client_salt']
        password_hash = self.sdk._hash_password(TEST_USER_PASSWORD, client_salt)
        data = {'password': password_hash}
        res = self.client.delete(url, data)
        self.assertEqual(res.status_code, 404)

        user = User.objects.get(id=self.sdk.user_id)
        secret = self.sdk.create_secret("foo", {}, {"password": "1"})
        group = self.sdk.create_group("group", 'group')
        self.sdk.add_group_to_secret(group['id'], secret['id'])
        secret = self.sdk.create_secret(
            "foo", {}, {"password": "1"}, group_id=group['id'])
        self.sdk.create_secret(
            "foo", {}, {"password": "1"})
        # Now we have 3 secrets - 2 of which has groups attached
        self.assertEqual(SecretThrough.objects.filter(user=user).count(), 2)
        self.assertEqual(Secret.objects.all().count(), 3)

        url = reverse('users-detail', args=[user.id])
        res = self.client.delete(url, data)
        self.assertEqual(res.status_code, 204)
        self.assertEqual(SecretThrough.objects.filter(user=user).count(), 0)
        # The user had 2 group and 1 personal secret.
        # We expect the personal secret to be destroyed
        self.assertEqual(Secret.objects.all().count(), 2)

    def test_login(self):
        email = TEST_USER_USERNAME
        password = TEST_USER_PASSWORD
        login = self.sdk.log_in(email, password)
        self.assertEqual(
            login['user']['id'],
            User.objects.get(email=email).id
        )

        self.sdk = PassitTestClient(client=APIClient())
        with self.assertRaises(PassitAPIException):
            self.sdk.log_in(TEST_USER_USERNAME, 'nope')

    def test_lookup_user_id(self):
        url = reverse('lookup-user-id')

        data = {
            "email": "noone@example.com",
        }
        res = self.client.post(url, data)
        self.assertEqual(res.status_code, 403)

        login = self.sdk.log_in(TEST_USER_USERNAME, TEST_USER_PASSWORD)

        data = {
            "email": "noone@example.com",
        }
        res = self.client.post(url, data)
        self.assertEqual(res.status_code, 404)

        data = {
            "email": TEST_USER_USERNAME,
        }
        res = self.client.post(url, data)
        self.assertEqual(res.status_code, 200)
        self.assertEqual(res.data['id'], 2)


class TestGroupSDK(PassitTestMixin, APITestCase):
    fixtures = ['users.json']

    def test_add_user_to_group(self):
        group_name = "My Group"
        slug = "my-group"
        invite_user = User.objects.filter(is_staff=False).last()
        self.sdk.log_in(TEST_USER_USERNAME, TEST_USER_PASSWORD)
        group_id = self.sdk.create_group(group_name).get('id')
        group = Group.objects.get(pk=group_id)
        self.assertEqual(group.slug, slug)
        self.assertEqual(group.users.count(), 1)
        self.sdk.add_user_to_group(group_id, invite_user.id, invite_user.email)
        self.assertEqual(group.users.count(), 2)


class TestGroupAPI(PassitTestMixin, APITestCase):
    fixtures = ['users.json']

    def test_group(self):
        group_name = "My Group"
        slug = "my-group"
        url = reverse('groups-list')
        user = User.objects.filter(is_staff=False).first()

        with self.assertRaises(PassitAuthenticationRequired):
            self.sdk.create_group(group_name, slug)

        self.sdk.log_in(TEST_USER_USERNAME, TEST_USER_PASSWORD)

        group_id = self.sdk.create_group(group_name, slug).get('id')

        group = Group.objects.get(pk=group_id)
        self.assertEqual(group.name, group_name)

        group_user = group.groupuser_set.get(user=user)
        self.assertEqual(group.name, group_name)
        self.assertEqual(group_user.is_group_admin, True)

        # GET
        res = self.client.get(url)
        self.assertContains(res, group_name)

        # user2 can't see user1's groups
        user2 = User.objects.filter(is_staff=False).exclude(pk=user.pk).first()
        self.client.force_authenticate(user=user2)
        res = self.client.get(url)
        self.assertNotContains(res, group_name)

        # PATCH existing group should be allowed
        self.client.force_authenticate(user=user)
        data = {
            "name": "My Edited Group"
        }
        url = reverse('groups-detail', args=[group.pk])
        res = self.client.patch(url, data)
        self.assertContains(res, data['name'])

        # PATCH shouldn't be allowed if we are no longer a group admin
        group_user.is_group_admin = False
        group_user.save()
        res = self.client.patch(url, data)
        self.assertEqual(res.status_code, 403)

        # GET should still work without being admin
        res = self.client.get(url)
        self.assertEqual(res.status_code, 200)

    def test_group_users(self):
        user = User.objects.filter(is_staff=False).first()
        self.client.force_authenticate(user=user)
        user2 = User.objects.filter(is_staff=False).exclude(pk=user.pk).first()
        group_with_admin = mommy.make(
            Group,
        )
        fake_ciphertext = "x" * 500
        group_with_admin_user = mommy.make(
            GroupUser,
            group=group_with_admin,
            user=user,
            is_group_admin=True,
            private_key_ciphertext=fake_ciphertext,
            key_ciphertext=fake_ciphertext
        )
        group_no_admin = mommy.make(
            Group,
        )
        group_no_admin_user = mommy.make(
            GroupUser,
            group=group_no_admin,
            user=user,
            is_group_admin=False,
            private_key_ciphertext=fake_ciphertext,
            key_ciphertext=fake_ciphertext
        )
        group_no_users = mommy.make(Group)

        # Not part of this group, no access
        url = reverse('group-users-list', args=[group_no_users.pk])
        res = self.client.get(url)
        self.assertEqual(res.status_code, 403)

        # Group with membership
        url = reverse('group-users-list', args=[group_no_admin.pk])
        res = self.client.get(url)
        self.assertContains(res, user.id)
        self.assertContains(res, group_no_admin.id)

        # Get detail
        url = reverse('group-users-detail',
                      args=[group_no_admin.pk, group_no_admin_user.pk])
        res = self.client.get(url)
        self.assertContains(res, user.id)
        self.assertContains(res, group_no_admin.id)

        # Cannot add user to group when not group admin
        data = {
            "user": user2.pk,
            "user_email": user2.email,
            "is_group_admin": False,
            "key_ciphertext":  fake_ciphertext,
            "private_key_ciphertext": fake_ciphertext,
        }
        url = reverse('group-users-list', args=[group_no_admin.pk])
        res = self.client.post(url, data)
        self.assertEqual(res.status_code, 403)

        # Can add user to group when group admin
        url = reverse('group-users-list', args=[group_with_admin.pk])
        res = self.client.post(url, data)
        self.assertEqual(res.status_code, 201)
        group_user_id = res.data['id']

        # Cannot add same user twice
        res = self.client.post(url, data)
        self.assertEqual(res.status_code, 400)

        # Can edit group membership admin status when group admin
        data = {
            "is_group_admin": True,
        }
        url = reverse('group-users-detail',
                      args=[group_with_admin.pk, group_user_id])
        res = self.client.patch(url, data)
        self.assertContains(res, user2.id)
        self.assertContains(res, 'true')

        # Cannot change the user field
        data = {
            "user": 99999
        }
        url = reverse('group-users-detail',
                      args=[group_with_admin.pk, group_user_id])
        res = self.client.patch(url, data)
        self.assertEqual(res.data['user'], user2.id)

        # Can delete membership
        self.client.delete(url)
        self.assertFalse(
            group_with_admin.groupuser_set.filter(user=user2).exists()
        )

    def test_invalid_group_invite(self):
        """
        user1 is in a group
        user3 invites user 2 to the group
        user3 is not in the group, so they get a 403
        """
        user1 = mommy.make(User)
        user2 = mommy.make(User)
        user3 = mommy.make(User)
        group = mommy.make(Group)
        groupuser = mommy.make(
            GroupUser,
            user=user1,
            group=group,
            key_ciphertext="a",
            private_key_ciphertext="a")
        url = reverse('group-users-list', args=[group.id])
        data = {
            "user": user2.id,
            "key_ciphertext": FAKE_CIPHERTEXT,
            "private_key_ciphertext": FAKE_CIPHERTEXT,
        }
        self.client.force_login(user3)
        res = self.client.post(url, data)
        self.assertEquals(res.status_code, 403)

    def do_group_invite(self, invitee=None):
        """ Create and return 2 new users - user1 invites user2 to a new group
        """
        if invitee is None:
            user1 = mommy.make(User)
        else:
            user1 = invitee
        user2 = mommy.make(User)
        group = mommy.make(Group)
        groupuser = mommy.make(
            GroupUser,
            user=user1,
            group=group,
            key_ciphertext="a",
            private_key_ciphertext="a")
        url = reverse('group-users-list', args=[group.id])
        data = {
            "user": user2.id,
            "user_email": user2.email,
            "key_ciphertext": FAKE_CIPHERTEXT,
            "private_key_ciphertext": FAKE_CIPHERTEXT,
        }
        self.client.force_login(user1)
        res = self.client.post(url, data)
        self.assertEquals(res.status_code, 201)
        return user1, user2, group

    @override_settings(IS_PRIVATE_ORG_MODE=False)
    def test_group_invites(self):
        """ When not in private org mode - adding a user to a group should 
        invite them."""
        user1, user2, group = self.do_group_invite()
        self.assertEquals(
            GroupUserInvite.objects.filter(inviter=user1).count(),
            1
        )
        groupuserinvites = GroupUserInvite.objects.filter(
            invitee_groupuser__user=user2)
        self.assertEqual(groupuserinvites.count(), 1)

    @override_settings(IS_PRIVATE_ORG_MODE=True)
    def test_no_group_invites_in_private_mode(self):
        """ When in private org mode - adding a user should just do it - not
        invite them """
        user1, user2, group = self.do_group_invite()
        self.assertEqual(GroupUserInvite.objects.count(), 0)

    @override_settings(IS_PRIVATE_ORG_MODE=False)
    def test_viewing_who_accepted_an_invite_or_not(self):
        """ The api should indicate when a group user is pending accepting the
        invite or already accepted. """
        user1, user2, group = self.do_group_invite()
        # Make a few extra
        for _ in range(5): self.do_group_invite(invitee=user1)
        # user1 needs to know which group members accepted their invite
        self.client.force_login(user1)
        url = reverse('groups-list')
        # Optimize this - don't lookup each invite one at a time
        with self.assertNumQueries(5):
            res = self.client.get(url)
        data_group = res.data[0]
        group_users = data_group['groupuser_set']
        group_user1 = next(x for x in group_users if x['user'] == user1.id)
        group_user2 = next(x for x in group_users if x['user'] == user2.id)
        self.assertEqual(group_user1['is_invite_pending'], False)
        self.assertEqual(group_user2['is_invite_pending'], True)

    def test_accept_invite(self):
        """ A user can accept an invite
        An invite is "accepted" when it is deleted from the database
        """ 
        user1, user2, group = self.do_group_invite()
        self.client.force_login(user2)
        url = reverse('accept-group-invite')
        groupuser = group.groupuser_set.get(user=user2)
        self.assertEqual(groupuser.groupuserinvite_set.count(), 1)
        res = self.client.post(url, {"id": groupuser.id})
        self.assertEqual(res.status_code, 200)
        self.assertEqual(groupuser.groupuserinvite_set.count(), 0)
        self.assertTrue(group.groupuser_set.filter(user=user2).exists())
        # Try again, shouldn't exist now and should 404
        res = self.client.post(url, {"id": groupuser.id})
        self.assertEqual(res.status_code, 404)
        # Try something dumb..
        res = self.client.post(url, {"lol": 1})
        self.assertEqual(res.status_code, 400)
        # Try a invite I don't have access to
        user3, user4, group2 = self.do_group_invite()
        groupuser2 = group2.groupuser_set.get(user=user4)
        res = self.client.post(url, {"id": groupuser2.id})
        self.assertEqual(res.status_code, 404)

    def test_reject_invite(self):
        """ A user can reject an invite
        An invite is "rejected" when it and the groupuser are both deleted
        from the database.
        """
        user1, user2, group = self.do_group_invite()
        self.client.force_login(user2)
        url = reverse('reject-group-invite')
        groupuser = group.groupuser_set.get(user=user2)
        self.assertEqual(groupuser.groupuserinvite_set.count(), 1)
        res = self.client.post(url, {"id": groupuser.id})
        self.assertEqual(res.status_code, 200)
        self.assertEqual(groupuser.groupuserinvite_set.count(), 0)
        self.assertFalse(group.groupuser_set.filter(user=user2).exists())
        # Try again, shouldn't exist now and should 404
        res = self.client.post(url, {"id": groupuser.id})
        self.assertEqual(res.status_code, 404)
        # Try a invite I don't have access to
        user3, user4, group2 = self.do_group_invite()
        groupuser2 = group2.groupuser_set.get(user=user4)
        res = self.client.post(url, {"id": groupuser2.id})
        self.assertEqual(res.status_code, 404)

    def test_contacts(self):
        url = reverse('contacts-list')
        self.sdk.log_in(TEST_USER_USERNAME, TEST_USER_PASSWORD)
        invite_user = User.objects.filter(is_staff=False).last()

        res = self.client.get(url)
        self.assertEqual(len(res.data), 0)

        group_id = self.sdk.create_group("test", "test").get('id')
        self.sdk.add_user_to_group(group_id, invite_user.id, invite_user.email)

        res = self.client.get(url)
        self.assertEqual(len(res.data), 2)  # Self and invite_user

    @override_settings(IS_TEST_MODE=False)
    def test_contants_email_not_confirmed(self):
        """ Unconfirmed email permission errors should show a different detail than 
        unauthenticated ones so that the client can give a helpful message """
        url = reverse('contacts-list')
        email = 'notconfirmed@aa.aa'
        password = TEST_USER_PASSWORD

        res = self.client.get(url)
        self.assertEqual(res.status_code, 403)
        self.assertEqual(res.data['detail'], "Authentication credentials were not provided.")

        self.sdk.log_in(email, password)

        res = self.client.get(url)
        self.assertEqual(res.status_code, 403)
        self.assertEqual(res.data['detail'], "User's email is not confirmed.")

    def test_contacts_private_org(self):
        with self.settings(IS_PRIVATE_ORG_MODE=True):
            url = reverse('contacts-list')
            self.sdk.log_in(TEST_USER_USERNAME, TEST_USER_PASSWORD)

            res = self.client.get(url)
            # Always show all users
            num_users = User.objects.all().count()
            self.assertEqual(len(res.data), num_users)
