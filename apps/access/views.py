from django.shortcuts import get_object_or_404
from django.conf import settings
from rest_framework import viewsets, permissions, generics, status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.throttling import UserRateThrottle, AnonRateThrottle
from .serializers import (
    UserSerializer, GroupSerializer, DetailedGroupSerializer,
    GroupUserSerializer, GroupUserUpdateSerializer,
    UserPublicAuthSerializer, UsernameAvailableSerializer,
    PublicUserSerializer, UserPublicKeySerializer, ContactSerializer,
    UserIdSerializer, LookupUserIdSerializer, AcceptRejectGroupUserSerializer,
    PasswordSerializer
)
from .models import GroupUser, Group, User, GroupUserInvite

from apps.auth.permissions import HasVerifiedEmail


class Burst20MinUser(UserRateThrottle):
    rate = '20/minute'
    scope = 'burst'


class Sustained500DayUser(UserRateThrottle):
    rate = '500/day'
    scope = 'sustained'


class Burst15MinAnon(AnonRateThrottle):
    rate = '15/minute'
    scope = 'burst'


class Sustained200DayAnon(AnonRateThrottle):
    rate = '200/day'
    scope = 'sustained'


class PingView(APIView):
    """ This exists to let a client verify the server exists  """
    permission_classes = [permissions.AllowAny]

    def get(self, request):
        return Response({"ping": "pong"})


class UserPublicAuthView(generics.RetrieveAPIView):
    """ Request a users publically known client side hashing
    algorithym, iteractions, and salt. No authentication required. """
    permission_classes = [permissions.AllowAny]
    queryset = User.objects.all()
    serializer_class = UserPublicAuthSerializer
    lookup_field = 'email'
    throttle_classes = (
        Burst20MinUser,
        Sustained500DayUser,
        Burst15MinAnon,
        Sustained200DayAnon,)


class UserLookUpView(generics.RetrieveAPIView):
    """ Look up a users id, firstname and lastname by an email """
    queryset = User.objects.all()
    serializer_class = PublicUserSerializer
    lookup_field = 'email'
    throttle_classes = (
        Burst20MinUser,
        Sustained500DayUser,
        Burst15MinAnon,
        Sustained200DayAnon,)


class MyContactsViewSet(viewsets.ReadOnlyModelViewSet):
    """ Returns all users who are in any group the user is a member of.
    If private org mode is enabled, return all registered users.
    """
    serializer_class = ContactSerializer

    def get_queryset(self):
        qs = User.objects.all()
        if settings.IS_PRIVATE_ORG_MODE:
            # All users
            return qs
        qs = qs.filter(groups__users=self.request.user)
        return qs


class UserPublicKeyView(generics.RetrieveAPIView):
    queryset = User.objects.all()
    serializer_class = UserPublicKeySerializer
    lookup_field = 'id'


class UsernameAvailableView(APIView):
    """ Check if username is available for new user.

    Accepts GET requests like `/api/username-available/email@example.com/`

    If username is available returns `{"available": true}`
    """
    permission_classes = [permissions.AllowAny]
    throttle_classes = (Burst20MinUser, Sustained500DayUser, Burst15MinAnon,
                        Sustained200DayAnon)
    serializer_class = UsernameAvailableSerializer

    def get(self, request, format=None, **kwargs):
        serializer = self.serializer_class(data=kwargs)
        if serializer.is_valid():
            email = serializer.data.get('email')

            if User.objects.filter(email=email).exists():
                return Response({"available": False})
            return Response({"available": True})
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class LookupUserIdView(APIView):
    """ If a exact email match is known, return user id. Otherwise return 404
    This is helpful when trying to add an existing user to a group you never
    interacted with before.
    Rate limit to mitigate skimming emails.
    """
    throttle_classes = (Burst20MinUser, Sustained500DayUser, Burst15MinAnon,
                        Sustained200DayAnon)
    serializer_class = LookupUserIdSerializer

    def post(self, request, format=None):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            email = serializer.data.get('email')
            user = get_object_or_404(User, email=email)
            id_serializer = UserIdSerializer(instance=user)
            return Response(id_serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class UserViewSet(viewsets.ModelViewSet):
    """ Create and get users.

    To create:

    1. Generate new key pair client side.
    2. hash the password
    3. Send hashed password and encrypted private key.
    """
    serializer_class = UserSerializer
    permission_classes = [permissions.AllowAny]  # For making new users

    def get_queryset(self):
        """ Return only current user or none """
        user = self.request.user
        if user.is_authenticated():
            return User.objects.filter(id=user.id)
        return User.objects.none()
    
    def destroy(self, request, *args, **kwargs):
        """ Destroy requires password """
        serializer = PasswordSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        if not request.user.check_password(serializer.data.get("password")):
            return Response({"password": ["Wrong password."]}, status=status.HTTP_400_BAD_REQUEST)
        return super().destroy(request, *args, **kwargs)


class IsGroupAdminOrReadOnly(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True
        return obj.groupuser_set.filter(
            user=request.user, is_group_admin=True,
        ).exists()


class GroupViewSet(viewsets.ModelViewSet):
    """ A group is what gives users permission to view secrets
    The detail view (/groups/<id>) will give some read only data related to
    users in this group.

    If you need to edit group memebership and admin status see
    Group Users `/api/groups/<group_id>/users/`

    Creating a new group will automatically assign the current user as an
    group admin member. As always encrypt data client side.

    ### How to create a new Group:

    1. Ensure you have your personal rsa keys
    2. Generate new RSA keys for group
    3. Generate new AES key for group
    4. Encrypt AES key using your own public RSA key
    5. Encrypt group RSA key using AES key
    6. Submit example

            {
                "name": "My Group",
                "public_key": "-----BEGIN PUBLIC KEY....",
                "key_ciphertext": "a3b7fff3...",
                "private_key_ciphertext": "a2112d5...",
            }
    """
    serializer_class = GroupSerializer
    permission_classes = [HasVerifiedEmail, IsGroupAdminOrReadOnly]

    def get_queryset(self):
        return self.request.user.groups.all(
            ).prefetch_related('groupuser_set__groupuserinvite_set')

    def get_serializer_class(self):
        # Give a more detailed result for just one object
        if self.action in ['retrieve', 'list']:
            return DetailedGroupSerializer
        return super().get_serializer_class()


class GroupUserPermission(permissions.BasePermission):
    def check_group_admin(self, user, group):
        """ Returns True if the user is an admin for the group """
        return group.groupuser_set.filter(
            user=user, is_group_admin=True,
        ).exists()

    def check_group_membership(self, user, group):
        """ Returns True if the user is in group """
        return group.groupuser_set.filter(
            user=user
        ).exists()

    def basic_user_checks(self, user):
        """ Inactive users can do nothing Staff users can do anything.
        This overides other checks.
        Return None if there is no override
        """
        if user.is_active is False:
            return False
        if user.is_staff is True:
            return True

    def has_object_permission(self, request, view, obj):
        perm = self.basic_user_checks(request.user)
        if perm is not None:
            return perm

        if request.method in permissions.SAFE_METHODS:
            return self.check_group_membership(request.user, obj.group)
        return self.check_group_admin(request.user, obj.group)

    def has_permission(self, request, view):
        group = view.get_group()

        if request.method in permissions.SAFE_METHODS:
            return self.check_group_membership(request.user, group)

        return self.check_group_admin(request.user, group)


class GroupUserViewSet(viewsets.ModelViewSet):
    """ Users in a group. Allows editing if current user is a group admin.

    Does not allow editing group or user when updating. For this
    please delete the object and make a new one.

    ## How to add a user

    1. First get the user's public key and the group's unencrypted private key
    (you must decrypt this client side).
    2. Generate a new AES key.
    3. Encrypt the AES key using the user's public key
    4. Encrypt the group private key using the AES key
    5. Submit everything in it's encrypted form. Never post plain text secrets
    to the server (Assume the server is evil and hates you at all times)
    """
    serializer_class = GroupUserSerializer
    permission_classes = [permissions.IsAuthenticated, HasVerifiedEmail, GroupUserPermission]

    def get_serializer_class(self):
        if self.request and self.request.method in ['PUT', 'PATCH']:
            return GroupUserUpdateSerializer
        return super().get_serializer_class()

    def get_group(self):
        return get_object_or_404(Group, pk=self.kwargs.get('id'))

    def get_queryset(self):
        group = self.get_group()
        user = self.request.user
        return GroupUser.objects.filter(
            group=group,
            group__users=user,
        )

    def perform_create(self, serializer):
        group = self.get_group()
        serializer.save(group=group)

    def perform_update(self, serializer):
        group = self.get_group()
        serializer.save(group=group)


class AcceptGroupInviteView(APIView):
    """ Accept a group invite. Send:

    `{"id": <groupuser_id>}`

    Expect a 200 if successful. No data will be returned.
    """
    permission_classes = [permissions.IsAuthenticated, HasVerifiedEmail]

    def post(self, request, format=None):
        serializer = AcceptRejectGroupUserSerializer(data=request.data)
        if serializer.is_valid():
            invite = get_object_or_404(
                GroupUserInvite,
                invitee_groupuser__pk=serializer.data['id'],
                invitee_groupuser__user=self.request.user
            )
            invite.delete()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class RejectGroupInviteView(APIView):
    """ Reject a group invite. Send:

    `{"id": <groupuser_id>}`

    Expect a 200 if successful. No data will be returned.
    """
    permission_classes = [permissions.IsAuthenticated, HasVerifiedEmail]

    def post(self, request, format=None):
        serializer = AcceptRejectGroupUserSerializer(data=request.data)
        if serializer.is_valid():
            groupuser = get_object_or_404(
                GroupUser,
                pk=serializer.data['id'],
                user=self.request.user
            )
            groupuser.delete()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
