from django.conf import settings
from rest_framework import permissions


class HasVerifiedEmail(permissions.BasePermission):
    """ Ensure user is email verified """
    message = "User's email is not confirmed."

    def has_permission(self, request, view):
        if settings.IS_TEST_MODE is True:
            return True
        return request.user and request.user.email_confirmed
