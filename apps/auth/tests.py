from django.core.urlresolvers import reverse
from django.urls import reverse
from django.test import override_settings
from rest_framework.test import APITestCase
from apps.access.models import User
from model_mommy import mommy
from knox.models import AuthToken
from .authentication import MAX_ATTEMPTS

from passit.test_client import PassitTestMixin

import base64
import pytz
import datetime

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
        'LOCATION': 'unique-snowflake',
    }
}


class AuthTestCase(PassitTestMixin, APITestCase):
    @override_settings(IS_TEST_MODE=False)
    def test_unconfirmed_email(self):
        """ An unconfirmed account should have limited access only """
        email = "testy@test.test"
        password = "12345678"

        self.sdk.sign_up(email, password)
        user = User.objects.get()

        url = reverse('secrets-list')
        self.client.force_login(user)

        res = self.client.get(url)
        self.assertEqual(res.status_code, 403)

    @override_settings(IS_TEST_MODE=True)
    def test_test_mode(self):
        """ The client e2e tests depend on the server operating in a test mode
        where email confirmation is disabled. We must ensure this is supported.
        """
        email = "testy@test.test"
        password = "12345678"

        self.sdk.sign_up(email, password)
        user = User.objects.get()

        url = reverse('secrets-list')
        self.client.force_login(user)

        res =self.client.get(url)
        self.assertEqual(res.status_code, 200)
    
    @override_settings(CACHES=CACHES)
    def test_limit_login_attempts(self):
        """ Prevent brute force server authetication by limitnig attempts per user """
        url = reverse('knox_login')
        auth_headers = {
            'HTTP_AUTHORIZATION': 'Basic ' + base64.b64encode('username:password'.encode()).decode(),
        }
        for _ in range(0, MAX_ATTEMPTS):
            res = self.client.post(url, **auth_headers)
            self.assertEqual(res.status_code, 401)
            self.assertEqual(res.content, b'{"detail":"Invalid username/password."}')
        res = self.client.post(url, **auth_headers)
        self.assertEqual(res.status_code, 401)
        self.assertEqual(res.content, b'{"detail":"Locked out: too many authentication failures"}')

    def test_login(self):
        """ Since the knox view is customized, test it """
        user = mommy.make(User)
        user.set_password('hello')
        user.save()
        url = reverse('knox_login')
        auth_headers = {
            'HTTP_AUTHORIZATION': 'Basic ' + base64.b64encode(f'{user.email}:hello'.encode()).decode(),
        }
        res = self.client.post(url, **auth_headers)
        self.assertEqual(res.status_code, 200)
        self.assertTrue(AuthToken.objects.exists())

    def test_login_expires_validation(self):
        """ Passing a invalid expires should return 400 """
        user = mommy.make(User)
        user.set_password('hello')
        user.save()
        url = reverse('knox_login')
        auth_headers = {
            'HTTP_AUTHORIZATION': 'Basic ' + base64.b64encode(f'{user.email}:hello'.encode()).decode(),
        }
        url = reverse('knox_login')
        data = {"expires": "fail"}
        res = self.client.post(url, data, **auth_headers)
        self.assertEqual(res.status_code, 400)

    def test_login_expires(self):
        """ Test being able to set the expires time (in hours) """
        user = mommy.make(User)
        user.set_password('hello')
        user.save()
        url = reverse('knox_login')
        auth_headers = {
            'HTTP_AUTHORIZATION': 'Basic ' + base64.b64encode(f'{user.email}:hello'.encode()).decode(),
        }
        hours = 4
        data = {'expires': hours}
        res = self.client.post(url, data, **auth_headers)
        self.assertEqual(res.status_code, 200)

        token = AuthToken.objects.get()
        delta = token.expires - datetime.datetime.now(pytz.utc)
        delta_hours = round(delta.seconds / 60 / 60)
        self.assertAlmostEqual(delta_hours, hours)
