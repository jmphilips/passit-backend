from django.contrib.auth.signals import user_logged_in
from rest_framework.response import Response
from rest_framework.exceptions import ValidationError
from knox.models import AuthToken
from knox.views import LoginView as KnoxLoginView
from knox.settings import knox_settings
import datetime

from .authentication import QuietBasicAuthentication

class LoginView(KnoxLoginView):
    authentication_classes = [QuietBasicAuthentication]

    def post(self, request, format=None):
        """ Accepts optional param - expires - int - number of hours until token expires """
        expires = request.data.get('expires', None)
        # Default to global settings
        if expires is None: 
            return super().post(request, format)

        try:
            expires = datetime.timedelta(hours=int(expires))
        except ValueError:
            raise ValidationError("expires must a number")
        token = AuthToken.objects.create(request.user, expires=expires)
        user_logged_in.send(sender=request.user.__class__, request=request, user=request.user)
        UserSerializer = knox_settings.USER_SERIALIZER
        context = {'request': self.request, 'format': self.format_kwarg, 'view': self}
        return Response({
            'user': UserSerializer(request.user, context=context).data,
            'token': token,
        })
