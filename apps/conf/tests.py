from django.urls import reverse
from rest_framework.test import APITestCase


class ConfTestCase(APITestCase):
    def test_conf(self):
        url = reverse('conf')
        response = self.client.get(url)
        self.assertFalse(response.data['IS_DEMO'])
        self.assertFalse(response.data['IS_PRIVATE_ORG_MODE'])
