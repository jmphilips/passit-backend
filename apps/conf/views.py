from django.conf import settings
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import permissions


class ConfView(APIView):
    """ A way for the client to get global server configuration
    such as demo mode
    """
    permission_classes = [permissions.AllowAny]

    def get(self, request, format=None):
        data = {
            'IS_DEMO':  settings.IS_DEMO,
            'IS_PRIVATE_ORG_MODE': settings.IS_PRIVATE_ORG_MODE,
        }
        return Response(data)
