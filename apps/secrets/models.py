from django.db import models
from django.db.models import Q
from django.contrib.postgres.fields import HStoreField
from django.core.exceptions import ValidationError
from django.core.exceptions import NON_FIELD_ERRORS


class SecretManager(models.Manager):
    def mine(self, user):
        return self.get_queryset().filter(
            Q(secret_through_set__user=user) | Q(secret_through_set__group__users=user)
        )


class Secret(models.Model):
    """ One secret. Normalized. Contains no actual secret data.
    Secret data should reference this and be denormalized.
    type: Defines the schema used for visible and hidden data
    data: Any non critical data. Unencrypted. For example a URL.
    """
    name = models.CharField(max_length=200)
    type = models.CharField(max_length=200, default="website")
    data = HStoreField(blank=True)

    objects = SecretManager()

    def __str__(self):
        return self.name


class SecretThroughManager(models.Manager):
    def mine(self, user):
        return self.get_queryset().filter(
            Q(user=user) | Q(group__users=user)
        )


class SecretThrough(models.Model):
    """ A copy of the secret data for each group member
    hidden_data - Critical data such as a password. Encrypted with
    symmetric encryption.
    """
    secret = models.ForeignKey(Secret, related_name="secret_through_set")
    user = models.ForeignKey('access.User', blank=True, null=True)
    group = models.ForeignKey('access.Group', blank=True, null=True)
    key_ciphertext = models.CharField(max_length=1024)
    data = HStoreField(blank=True)

    objects = SecretThroughManager()

    class Meta:
        unique_together = ('secret', 'user', 'group')

    def validate_unique(self, *args, **kwargs):
        super().validate_unique(*args, **kwargs)
        if not self.user and not self.group:
            raise ValidationError(
                {NON_FIELD_ERRORS: ["A secret must have a user or group"]}
            )
        if self.user and self.group:
            raise ValidationError(
                {NON_FIELD_ERRORS: ["A secret cannot have both user and group"]}
            )

    def get_public_key(self):
        if self.user:
            return self.user.public_key
        elif self.group:
            return self.group.public_key
