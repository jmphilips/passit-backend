from django.core.exceptions import SuspiciousOperation, ObjectDoesNotExist
from rest_framework import serializers
from .models import Secret, SecretThrough
from .exceptions import ChangePasswordConflictException
from ..access.models import User, Group, GroupUser
import hashlib


class SecretThroughSerializer(serializers.ModelSerializer):
    group = serializers.PrimaryKeyRelatedField(
        required=False,
        allow_null=True,
        default=None,
        queryset=Group.objects.all(),
    )
    public_key = serializers.SerializerMethodField()
    is_mine = serializers.SerializerMethodField()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if 'request' in self.context and self.context['view'].request:
            user = self.context['view'].request.user
            self.user_id = user.id
            self.my_groups = user.groups.values_list('id', flat=True)

    class Meta:
        model = SecretThrough
        fields = (
            'id', 'group', 'key_ciphertext', 'data', 'public_key',
            'is_mine'
        )

    def get_is_mine(self, obj):
        if hasattr(self, 'user_id'):
            if obj.group_id in self.my_groups or obj.user_id == self.user_id:
                return True
            return False

    def get_public_key(self, obj):
        if hasattr(obj, 'group'):
            if obj.group:
                return obj.group.public_key
            elif obj.user:
                return obj.user.public_key

    def update(self, instance, validated_data):
        secret_data = validated_data.pop('secret')
        secret = instance.secret

        secret.name = secret_data.get(
            'name',
            secret.name
        )
        secret.type = secret_data.get(
            'type',
            secret.type
        )
        secret.visible_data = secret_data.get(
            'data',
            secret.visible_data
        )
        secret.save()

        if (
            validated_data.get('owner') and
            instance.owner != validated_data['owner']
        ):
            raise SuspiciousOperation('Secrets api cannot change ownership')

        hidden_data = validated_data.get('data')
        if hidden_data is None or hidden_data == instance.hidden_data:
            # Nothing else to update
            return instance

        # We actually have to update EVERY secret's private data now
        for secret in secret.secret_through_set.all():
            # Only update hidden data. Other fields can't be changed.
            secret.set_hidden_data(hidden_data)
            secret.save()

            # We want to return the updated instance
            if secret.id == instance.id:
                my_secret = secret

        return my_secret


class SecretThroughHashUpdateSerializer(SecretThroughSerializer):
    """ Used when we need to confirm the client's last version is the same
    as the servers last verio. Checks hash of key_ciphertext """
    hash = serializers.RegexField(r"^[0-9a-fA-F]*")

    class Meta:
        model = SecretThrough
        fields = (
            'id', 'group', 'key_ciphertext', 'data', 'public_key',
            'is_mine', 'hash'
        )


class SecretSerializer(serializers.ModelSerializer):
    secret_through_set = SecretThroughSerializer(many=True)

    class Meta:
        model = Secret
        fields = ('id', 'name', 'type', 'data', 'secret_through_set')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # Pass context to nested serializer
        self.fields['secret_through_set'] = SecretThroughSerializer(
            many=True, context=self.context)

    def create(self, validated_data):
        secret_through_set_data = validated_data.pop('secret_through_set')
        secret = Secret.objects.create(**validated_data)
        for secret_through_data in secret_through_set_data:
            if secret_through_data.get('group'):
                user = None
            else:
                user = self.context['request'].user
            SecretThrough.objects.create(
                secret=secret,
                user=user,
                **secret_through_data)
        return secret

    def update(self, instance, validated_data):
        instance.name = validated_data.get('name', instance.name)
        instance.type = validated_data.get('type', instance.type)
        instance.data = validated_data.get('data', instance.data)

        if self.partial:
            # Can't just use keys() because it contains secrets and data
            # which may not be part of the partial update
            fields = []
            if validated_data.get('name'):
                fields.append('name')
            if validated_data.get('type'):
                fields.append('type')
            if validated_data.get('data'):
                fields.append('data')
            instance.save(update_fields=fields)
            return instance

        secrets_data = validated_data.pop('secret_through_set')

        # Updating existing, add new, delete ones removed
        updated_throughs = []
        for secret_through_data in secrets_data:
            try:
                group = secret_through_data.get('group')
                secret_through = instance.secret_through_set.get(group=group)
                secret_through.key_ciphertext = secret_through_data.get(
                    'key_ciphertext')
                secret_through.data = secret_through_data.get('data')
                secret_through.save()
                updated_throughs.append(secret_through.id)
            except ObjectDoesNotExist:  # New object
                new_through = instance.secret_through_set.add(
                    **secret_through_data)
                updated_throughs.append(new_through.id)
        instance.secret_through_set.exclude(id__in=updated_throughs).delete()

        instance.save()
        return instance


class UserPasswordSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('client_salt', 'password', 'public_key', 'private_key')
        extra_kwargs = {'password': {'write_only': True}}

    def update(self, instance, validated_data):
        instance.set_password(validated_data['password'])
        instance.client_salt = validated_data['client_salt']
        instance.public_key = validated_data['public_key']
        instance.private_key = validated_data['private_key']
        instance.save()
        return instance


class GroupUserUpdateSerializer(serializers.ModelSerializer):
    hash = serializers.RegexField(
        r"^[0-9a-fA-F]*",
        help_text="hash of key_ciphertext")

    class Meta:
        model = GroupUser
        fields = ('id', 'user', 'group', 'is_group_admin', 'hash',
                  'key_ciphertext', 'private_key_ciphertext')


class ChangePasswordSerializer(serializers.Serializer):
    user = UserPasswordSerializer()
    old_password = serializers.CharField(required=True)
    secret_through_set = SecretThroughHashUpdateSerializer(many=True)
    # Must disable validators to avoid faulty DRF uniqueness checks.
    group_user_set = GroupUserUpdateSerializer(many=True, validators=[])

    def check_hash(self, instance, new_hash):
        existing_hash = hashlib.md5(
            instance.key_ciphertext.encode()
        ).hexdigest()
        if existing_hash != new_hash:
            raise ChangePasswordConflictException()
    
    def save(self, **kwargs):
        validated_data = dict(
            list(self.validated_data.items()) +
            list(kwargs.items())
        )
        user_data = validated_data.pop('user')
        user_serializer = UserPasswordSerializer(
            instance=self.context['user'],
            data=user_data,
        )
        if user_serializer.is_valid(raise_exception=True):
            user_success = user_serializer.save()

        for secret_through in self.context['secret_through_set']:
            secret_through_instance = SecretThrough.objects.get(
                id=secret_through['id'],
                user=self.context['user'],
            )
            secret_through_serializer = SecretThroughHashUpdateSerializer(
                instance=secret_through_instance,
                data=secret_through
            )

            self.check_hash(secret_through_instance, secret_through['hash'])
            secret_through_serializer.is_valid(raise_exception=True)

            secret_through_instance.data = secret_through['data']
            secret_through_instance.key_ciphertext = secret_through['key_ciphertext']
            secret_through_instance.save()

        for group_user in self.context['group_user_set']:
            group_user_instance = GroupUser.objects.get(
                id=group_user['id'],
                user=self.context['user']
            )
            group_user_serializer = GroupUserUpdateSerializer(
                instance=group_user_instance,
                data=group_user,
            )

            self.check_hash(group_user_instance, group_user['hash'])
            group_user_serializer.is_valid(raise_exception=True)

            group_user_instance.key_ciphertext = group_user['key_ciphertext']
            group_user_instance.private_key_ciphertext = group_user['private_key_ciphertext']
            group_user_instance.save()

        return user_success
