from django.test import Client
from model_mommy import mommy
from rest_framework.test import APITestCase
from ...access.models import User, Group, GroupUser
from ..models import Secret, SecretThrough
from ..views import SecretThroughViewSet


class TestSecretThroughViewSet(APITestCase):
    def test_create_secret_through(self):
        """
        user1 creates a group and puts a secret in that group
        user2 POSTS to that secret's API URL (/secrets/<id>/groups/)
            with valid data, is unsuccessful
        """
        user1 = mommy.make(User)
        user1_group = mommy.make(Group)
        user1_secret = mommy.make(Secret, data={"password": "123456"})
        user1_secret_through = mommy.make(SecretThrough, secret=user1_secret,
                                          user=user1, group=user1_group,
                                          data={"password": "123456"})
        user2 = mommy.make(User)
        user2_group = mommy.make(Group)
        user2_secret = mommy.make(Secret, data={"password": "asdfgh"})
        user2_secret_through = mommy.make(SecretThrough, secret=user2_secret,
                                          user=user2, group=user2_group,
                                          data={"password": "asdfgh"})
        user2_request = '{"id": ' + str(user2_secret.id) + ', "group": ' + str(user2_group.id) + ', "key_ciphertext": "a", "data": {"password": "zxcvbn"}, "public_key": "a", "is_mine": true}'

        client = Client()
        client.force_login(user2)

        post_to_users_secret = client.post("/api/secrets/" + str(user2_secret.id) + "/groups/",
                                           user2_request,
                                           content_type="application/json")

        self.assertEqual(post_to_users_secret.status_code, 201)

        post_to_another_users_secret = client.post("/api/secrets/" + str(user1_secret.id) + "/groups/",
                                                   user2_request,
                                                   content_type="application/json")

        self.assertEqual(post_to_another_users_secret.status_code, 403)

        delete_attempt = client.delete(f'/api/secrets/{user1_secret.id}/groups/{user1_secret_through.id}/')
        self.assertEqual(delete_attempt.status_code, 404)
