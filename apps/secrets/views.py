from django.core.exceptions import PermissionDenied
from django.shortcuts import get_object_or_404
from rest_framework import viewsets, mixins
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .serializers import (
    SecretSerializer, SecretThroughSerializer, ChangePasswordSerializer)
from .models import Secret, SecretThrough


class SecretViewSet(viewsets.ModelViewSet):
    """ Manage personal and group secrets.

    Secrets must be stored in a key value json structure like

        `{"password": "hunter2", "security answer": "The Moon"}`
    Note that nested json is not supported.

    ## Create or update a personal secret

    1. Ensure you have your own public key.
    2. Generate a new AES key. Do not reuse AES keys.
    3. Encrypt the new AES key with your public key.
    4. Encrypt each value in the key value structure. Example:

        `{"password": "cyphertext1", "security answer": "cyphertext2"}`

    5. Submit everything. Use POST for a new secret. Use PUT to update existing
    Example:

            {
                "name": "My site",
                "type": "website",
                "data": {"url": "www.example.com"},
                "secret_through_set": [
                    {
                        "hidden_data": {
                            "password": "cyphertext1",
                            "security answer": "cyphertext2"
                        },
                        "encrypted_key": "aes_key_ciphertext"
                    }
                ]
            }

    ## Create or update group secrets
    1. Get group(s) public key - using groups api.
    2. Generate a new AES key. Do not reuse AES keys.
    3. Encrypt the new AES key with the group's public keys.
    Do this for each group.
    4. Encrypt each value in the key value structure. See personal secret for
    example. Also do this for each group. If the secret is shared with five
    groups then you should have five different AES keys and five sets of secret
    ciphertext.
    5. Submit everything. Example:

            {
                "name": "My site",
                "type": "website",
                "data": {"url": "www.example.com"},
                "secret_through_set": [
                    {
                        "hidden_data": {
                            "password": "cyphertext1",
                            "security answer": "cyphertext2"
                        },
                        "encrypted_key": "aes_key_ciphertext",
                        "group": "3"
                    },
                    {
                        "hidden_data": {
                            "password": "cyphertext3",
                            "security answer": "cyphertext4"
                        },
                        "encrypted_key": "different_aes_key_ciphertext",
                        "group": "4"
                    }
                ]
            }
    """
    queryset = Secret.objects.all()
    serializer_class = SecretSerializer

    def get_queryset(self):
        return Secret.objects.mine(self.request.user).distinct()


class SecretThroughViewSet(mixins.CreateModelMixin,
                           mixins.ListModelMixin,
                           mixins.RetrieveModelMixin,
                           mixins.DestroyModelMixin,
                           viewsets.GenericViewSet):
    """ SecretThrough can also be thought of like GroupSecrets or UserSecrets.
    Create, list, and destroy. Can't be used to update. Updating would require
    remaking ALL secret throughs so that all users get the update.
    """
    serializer_class = SecretThroughSerializer

    def get_queryset(self):
        my_secrets = Secret.objects.mine(self.request.user)
        secret_id = self.kwargs.get('id')
        queryset = SecretThrough.objects.filter(secret__in=my_secrets)
        queryset = queryset.filter(secret_id=secret_id)
        return queryset

    def perform_create(self, serializer):
        my_secrets = Secret.objects.mine(self.request.user)
        secret_id = self.kwargs.get('id')

        if my_secrets.filter(id=secret_id).exists():
            secret = get_object_or_404(Secret, pk=self.kwargs.get('id'))
            serializer.save(secret=secret)
        else:
            raise PermissionDenied


class ChangePasswordView(APIView):
    def post(self, request, format=None):
        # Use context for data because validation strips id fields :(
        # DRF can't deal well with nested writable data
        serializer = ChangePasswordSerializer(
            data=request.data,
            context={
                'user': request.user,
                'secret_through_set': request.data['secret_through_set'],
                'group_user_set': request.data['group_user_set'],
            }
        )
        if serializer.is_valid():
            if not request.user.check_password(serializer.data.get("old_password")):
                return Response({"old_password": ["Wrong password."]}, status=status.HTTP_400_BAD_REQUEST)
            serializer.save()
            # Expire all sessions
            request.user.auth_token_set.all().delete()
            return Response({"success": True}, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
