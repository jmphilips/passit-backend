from django.conf.urls import include, url
from django.contrib import admin
from rest_framework import routers
from apps.access.views import (
    UserPublicAuthView, UserViewSet, GroupViewSet, GroupUserViewSet,
    UsernameAvailableView, UserLookUpView, UserPublicKeyView,
    MyContactsViewSet, PingView, LookupUserIdView, AcceptGroupInviteView,
    RejectGroupInviteView
)
from apps.secrets.views import (
    SecretViewSet, SecretThroughViewSet, ChangePasswordView)
from apps.conf.views import ConfView
from apps.confirm_email.views import (
    confirm_email_from_long_code,
    confirm_email_from_short_code,
    request_new_confirmation,
)

from rest_framework.decorators import (
    api_view, renderer_classes, permission_classes, authentication_classes)
from rest_framework import response, schemas
from rest_framework_swagger.renderers import OpenAPIRenderer, SwaggerUIRenderer


@api_view()
@permission_classes([])
@authentication_classes([])
@renderer_classes([OpenAPIRenderer, SwaggerUIRenderer])
def schema_view(request):
    generator = schemas.SchemaGenerator(title='Passit API')
    return response.Response(generator.get_schema())


router = routers.DefaultRouter()
router.register(r'users', UserViewSet, base_name='users')
router.register(r'contacts', MyContactsViewSet, base_name='contacts')
router.register(r'groups', GroupViewSet, base_name='groups')
router.register(r'groups/(?P<id>\d+)/users', GroupUserViewSet, base_name='group-users')
router.register(r'secrets', SecretViewSet, base_name='secrets')
router.register(r'secrets/(?P<id>\d+)/groups', SecretThroughViewSet, base_name='secret-groups')

urlpatterns = [
    #url(r'^admin/', admin.site.urls),
    url(r'^api/', include(router.urls)),
    url(r'^api/ping/$', PingView.as_view(), name="ping"),
    url(r'^api/lookup-user-id/$', LookupUserIdView.as_view(), name="lookup-user-id"),
    url(r'^api/user-public-auth/(?P<email>[\w.%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,63})', UserPublicAuthView.as_view(), name='user-public-auth'),
    url(r'^api/username-available/(?P<email>[\w.%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,63})', UsernameAvailableView.as_view(), name='username-available'),
    url(r'^api/user-lookup/(?P<email>[\w.%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,63})', UserLookUpView.as_view(), name='user-available'),
    url(r'^api/user-public-key/(?P<id>\d+)/$', UserPublicKeyView.as_view(), name='user-public-key'),
    url(r'^api/change-password/', ChangePasswordView.as_view(), name='change-password'),
    url(r'^api/accept-group-invite/$', AcceptGroupInviteView.as_view(), name='accept-group-invite'),
    url(r'^api/reject-group-invite/$', RejectGroupInviteView.as_view(), name='reject-group-invite'),
    url(r'^api/confirm-email-long-code/', confirm_email_from_long_code, name='confirm-email-long-code'),
    url(r'^api/confirm-email-short-code/', confirm_email_from_short_code, name='confirm-email-short-code'),
    url(r'^api/request-new-confirmation/', request_new_confirmation, name='request-new-confirmation'),
    url(r'^api/conf/', ConfView.as_view(), name='conf'),
    url(r'^api/auth/', include('apps.auth.urls')),  # https://james1345.github.io/django-rest-knox/auth/#global-usage-on-all-views
    url(r'^docs/$', schema_view),
]
