from django.core.urlresolvers import reverse
from django.template.defaultfilters import slugify
from simple_asym import Asym
from .exceptions import (
    PassitSDKException, PassitAPIException, PassitAuthenticationRequired)
from .hashing import hash_password, generate_salt
import base64
import typing
import hashlib


class PassitTestClient:
    """ Simulates a client app """
    user_id = None
    asym: Asym

    def __init__(self, client=None):
        """ Client should be DRF test client or requests """
        if client is None:
            import requests
            client = requests
        self.client = client
        self.asym = Asym()

    def is_username_available(self, email: str) -> bool:
        """ Check is a username (email is username) is available in back-end

        :return: Boolean True when username is available
        """
        try:
            url = reverse('username-available', args=[email])
        except:
            raise PassitSDKException('Invalid email')
        res = self.client.get(url)
        if res.status_code == 400:
            raise PassitAPIException(res, 'Invalid email')
        if res.status_code != 200:
            raise PassitAPIException(res, 'Unable to check username')
        return res.data.get('available')

    def sign_up(self,
                email: str,
                password: str,
                first_name=None,
                last_name=None) -> dict:
        """ Sign up for new account

        :return: User ID """
        # Hash the password "client" side first
        client_salt = self._generate_salt()
        password_hash = self._hash_password(password, client_salt)

        # Generate keys client side
        asym = Asym()
        private, public = asym.make_rsa_keys(password=password)

        data = {
            "email": email,
            "password": password_hash,
            "public_key": public,
            "private_key": private,
            "client_salt": client_salt,
        }

        url = reverse('users-list')
        res = self.client.post(url, data)
        if res.status_code != 201:
            raise PassitAPIException(res, 'Unable to check username')
        return res.data

    def log_in(self, username: str, password: str) -> dict:
        """ Log in user signing in with hashed version of password
        """
        url = reverse('user-public-auth', args=[username])
        res = self.client.get(url)
        if res.status_code != 200:
            raise PassitAPIException(res, 'Unable to get public user info')
        data = res.json()

        hashed_password = self._hash_password(password, data['client_salt'])

        auth = base64.b64encode(
            username.encode() + b":" + hashed_password.encode()
        ).decode()
        url = reverse('knox_login')
        res = self.client.post(url, HTTP_AUTHORIZATION="BASIC " + auth)
        if res.status_code != 200:
            raise PassitAPIException(res, 'Unable to log in')
        token_auth = "TOKEN " + res.data.get('token')
        self.client.credentials(HTTP_AUTHORIZATION=token_auth)

        public = res.data['user']['public_key']
        private = res.data['user']['private_key']

        self.asym.set_key_pair(public, private, password)
        self.user_id = res.data['user']['id']
        return res.data

    def create_group(self, name: str, slug=None) -> dict:
        """ Create a new Group

        @return: The new group's id
        """
        if slug is None:
            slug = slugify(name)
        if not self.user_id:
            raise PassitAuthenticationRequired()
        # Generate keys client side
        group_asym = Asym()
        private_key, public_key = group_asym.make_rsa_keys()
        group_asym.make_symmetric_key()
        encrypted_private_key = group_asym.encrypt(private_key)
        aes_key = group_asym.get_encrypted_symmetric_key(
            self.asym.key_pair['public_key'])

        url = reverse('groups-list')
        data = {
            "name": name,
            "slug": slug,
            "public_key": public_key,
            "key_ciphertext": aes_key,
            "private_key_ciphertext": encrypted_private_key,
        }
        res = self.client.post(url, data)
        if res.status_code != 201:
            raise PassitAPIException(res, 'Couldn\'t create group')
        return res.data

    def get_group(self, group_id: int) -> dict:
        """ Get a group object with a group_id """
        url = reverse('groups-detail', args=[group_id])
        res = self.client.get(url)
        if res.status_code != 200:
            raise PassitAPIException(res, 'Couldn\'t get group')
        return res.data

    def list_groups(self) -> list:
        """ Get all groups belonging to user """
        url = reverse('groups-list')
        res = self.client.get(url)
        if res.status_code != 200:
            raise PassitAPIException(res, 'Couldn\'t list groups')
        return res.data

    def add_user_to_group(self, group_id: int, user_id: int, user_email: str) -> dict:
        """ Add a new user to a group
        user_email is required so that users cannot guess id's to randomly add to groups
        """
        # Get group's private key
        group_private_key = self._get_group_private_key(group_id)
        # Generate new AES key
        asym = Asym()
        asym.make_symmetric_key()
        # Encrypt AES key with user's public key
        invitee_public_key = self._get_user_public_key(user_id)
        encrypted_group_aes_key = asym.get_encrypted_symmetric_key(
            invitee_public_key)
        encrypted_group_private_key = asym.encrypt(group_private_key)
        # POST data
        url = reverse('group-users-list', args=[group_id])
        data = {
            "user": user_id,
            "user_email": user_email,
            "key_ciphertext": encrypted_group_aes_key,
            "private_key_ciphertext": encrypted_group_private_key,
        }
        res = self.client.post(url, data)
        return res.data

    def remove_user_from_group(self,
                               group_id: int,
                               group_user_id: int) -> dict:
        """ Remove existing user from a group.
        :param group_id: Id of group we want to remove user from
        :param group_user_id: Id of the group membership (not the user id)
        :return: Deletion response from server
        """
        url = reverse('group-users-list', args=[group_id, group_user_id])
        res = self.client.delete(url, data)
        return res.data

    def create_secret(self,
                      name: str,
                      visible_data: dict,
                      secrets: dict,
                      group_id=None) -> dict:
        """ Create a new secret

        Example of data structure

        {
            "url": "www.example.com",
            "username": "user"
        }

        :param name: Name of the new secret
        :param visible_data: key, value dict of any unencrypted data
        :param secrets: key, value dict of any encrypted data
        :param group_id: Optional param for creating a secret that is part of a
        group
        :return: Secret
        """
        url = reverse('secrets-list')
        if group_id:
            group_public_key = self._get_group_public_key(group_id)
            encrypted_key, encrypted_secrets = self._encrypt_secrets(
                group_public_key,
                secrets,
            )
        else:
            encrypted_key, encrypted_secrets = self._encrypt_secrets(
                self.asym.key_pair['public_key'],
                secrets,
            )

        data = {
            "name": name,
            "type": "website",
            "data": visible_data,
            "secret_through_set": [
                {
                    "group": group_id,
                    "data": encrypted_secrets,
                    "key_ciphertext": encrypted_key,
                },
            ],
        }

        res = self.client.post(url, data, format='json')
        if res.status_code != 201:
            raise PassitAPIException(res, 'Couldn\'t create secret')
        return res.data

    def update_secret(self,
                      secret_id: int,
                      name=None,
                      visible_data=None,
                      secret_data=None,
                      group_id=None) -> dict:
        """ """
        url = reverse('secrets-detail', args=[secret_id])
        if secret_data is None:  # Just patch then
            patch_data = {}
            if name:
                patch_data['name'] = name
            if visible_data:
                patch_data['data'] = visible_data
            res = self.client.patch(url, patch_data, format='json')
            if res.status_code != 200:
                raise PassitAPIException(res, "Could't patch secret")
            return res.data

        secret = self.get_secret(secret_id)
        for secret_through in secret['secret_through_set']:
            encrypted_key, encrypted_secrets = self._encrypt_secrets(
                secret_through['public_key'],
                secret_data,
            )
            secret_through['data'] = encrypted_secrets
            secret_through['key_ciphertext'] = encrypted_key

        data = {
            "name": name,
            "type": "website",
            "data": visible_data,
            "secret_through_set": secret['secret_through_set'],
        }

        res = self.client.put(url, data, format='json')
        if res.status_code != 200:
            raise PassitAPIException(res, 'Couldn\'t update secret')
        return res.data

    def get_secret(self, secret_id: int) -> dict:
        """ Get a secret. Does not decrypt data
        """
        url = reverse('secrets-detail', args=[secret_id])
        res = self.client.get(url)
        if res.status_code != 200:
            raise PassitAPIException(res, 'Couldn\'t get secret')
        return res.data

    def list_secrets(self) -> list:
        """ Get list of secrets. Does not decrypt data """
        url = reverse('secrets-list')
        res = self.client.get(url)
        if res.status_code != 200:
            raise PassitAPIException(res, 'Couldn\'t get secrets')
        return res.data

    def decrypt_secret(self, secret: dict) -> dict:
        """
        Decrypts ciphertext from a secret.

        :param secret: Unique secret object as returned from api
        :return: Plaintext of encrypted data in key, value format
        """
        # Any secret should work, get the first one.
        key_pair: dict = None
        for secret_through in secret.get('secret_through_set'):
            if secret_through['is_mine']:
                if secret_through.get('group'):
                    private_key = self._get_group_private_key(
                        secret_through['group'])
                    key_pair = {
                        "public_key": secret_through['public_key'],
                        "private_key": private_key,
                    }
                break

        secrets = self._decrypt_secrets(
            secret_through.get('key_ciphertext'),
            secret_through.get('data'),
            key_pair,
        )
        return secrets

    def add_group_to_secret(self,
                            group_id: int,
                            secret: typing.Union[int, dict]) -> dict:
        """ Add group to secret

        :param group_id: Group we want to add secret to
        :param secret: Either secret id or the secret object itself (for
        optimization)
        :return: SecretThrough response object
        """
        if isinstance(secret, int):
            secret = self.get_secret(secret)

        secret_data = self.decrypt_secret(secret)

        group_public_key = self._get_group_public_key(group_id)
        encrypted_key, encrypted_secrets = self._encrypt_secrets(
            group_public_key,
            secret_data,
        )

        url = reverse('secret-groups-list', kwargs={'id': secret['id']})
        data = {
            "group": group_id,
            "key_ciphertext": encrypted_key,
            "data": encrypted_secrets,
        }

        res = self.client.post(url, data, format='json')
        if res.status_code != 201:
            raise PassitAPIException(res, 'Unable to add group to secret')
        return res.data

    def remove_group_from_secret(
        self, secret_id: int, secret_through_id: int
    ) -> None:
        url = reverse(
            'secret-groups-detail',
            kwargs={'id': secret_id, 'pk': secret_through_id},
        )
        res = self.client.delete(url)
        if res.status_code != 204:
            raise PassitAPIException(res, 'Unable to remove group from secret')

    def change_password(self, old_password: str, new_password: str) -> dict:
        """ Change password. This sdk function acts as one large
        transaction against the backend. It either all fails or all passes.
        Complex due to needing to change every private key for this user.

        Sends hash of each changed secret through - to verify it hasn't
        changed server side
        """
        # Get old password salt first
        url = reverse('users-list')
        res = self.client.get(url)
        old_client_salt = res.data[0]['client_salt']

        url = reverse('change-password')

        client_salt = self._generate_salt()
        old_password_hash = self._hash_password(old_password, old_client_salt)
        password_hash = self._hash_password(new_password, client_salt)

        # Generate new RSA key pair
        asym = Asym()
        private, public = asym.make_rsa_keys(password=new_password)
        user_data = {
            "client_salt": client_salt,
            "password": password_hash,
            "public_key": public,
            "private_key": private,
        }

        # Remake all personal secrets
        secrets_list = self.list_secrets()
        secret_through_set = []
        for secret in secrets_list:
            secret_data = self.decrypt_secret(secret)
            for secret_through in secret['secret_through_set']:
                if (
                    secret_through['is_mine'] is True and
                    not secret_through.get('group')
                ):
                    encrypted_key, encrypted_secrets = self._encrypt_secrets(
                        public,
                        secret_data,
                    )
                    # Hash old key ciphertext as a unique hash for the server
                    # to verify that our previous secret through matches the
                    # server's.
                    hash = hashlib.md5(
                        secret_through['key_ciphertext'].encode()
                    ).hexdigest()
                    secret_through['hash'] = hash
                    secret_through['data'] = encrypted_secrets
                    secret_through['key_ciphertext'] = encrypted_key
                    secret_through_set += [secret_through]

        groups_list = self.list_groups()
        group_user_set = []
        for group in groups_list:
            group_private_key = self._get_group_private_key(group)
            for group_user in group['groupuser_set']:
                if group_user['user'] == self.user_id:
                    asym = Asym()
                    asym.make_symmetric_key()
                    # Encrypt AES key with user's public key
                    encrypted_group_aes_key = asym.get_encrypted_symmetric_key(
                        public)
                    encrypted_group_private_key = asym.encrypt(group_private_key)
                    hash = hashlib.md5(
                        group['my_key_ciphertext'].encode()
                    ).hexdigest()
                    group_user['hash'] = hash
                    group_user['key_ciphertext'] = encrypted_group_aes_key
                    group_user['private_key_ciphertext'] = encrypted_group_private_key
                    group_user_set.append(group_user)

        data = {
            "user": user_data,
            "old_password": old_password_hash,
            "secret_through_set": secret_through_set,
            "group_user_set": group_user_set,
        }
        res = self.client.post(url, data, format='json')
        if res.status_code != 200:
            raise PassitAPIException(res, 'Couldn\'t change password')
        return res.data

    @staticmethod
    def _hash_password(password: str, salt: str) -> str:
        """ Hash a password with salt, alg, and number of iterations

        :rtype: hashed password """
        return hash_password(password, salt)[0]

    @staticmethod
    def _generate_salt() -> str:
        """
        :return: Random salt in hex format of length 16"""
        return generate_salt()

    def _get_user_public_key(self, user_id: int) -> str:
        """ Get another user's public key

        :return: Public Key
        """
        url = reverse('user-public-key', args=[user_id])
        res = self.client.get(url)
        if res.status_code != 200:
            raise PassitAPIException(res, 'Unable to get user public key')
        return res.data.get('public_key')

    def _get_group_public_key(self, group_id: int) -> str:
        """ Return public key for a group """
        url = reverse('groups-detail', args=[group_id])
        res = self.client.get(url)
        if res.status_code != 200:
            raise PassitAPIException(res, 'Unable to get group public key')
        return res.data['public_key']

    def _get_group_private_key(self, group: typing.Union[dict, int]) -> bytes:
        """ Mock client gets the group private key

        :param group: Group object or group id. Note if passing id this
        requires a network connection
        :return: Private RSA Key
        """
        if isinstance(group, int):
            url = reverse('groups-detail', args=[group])
            group = self.client.get(url).data
        group = typing.cast(dict, group)

        group_aes = group['my_key_ciphertext']
        group_private_key = group['my_private_key_ciphertext']

        asym = Asym()
        asym.key_pair = self.asym.key_pair
        asym.set_symmetric_key_from_encrypted(group_aes)
        return asym.decrypt(group_private_key)

    def _decrypt_secrets(
        self,
        encrypted_key: str,
        encrypted_secrets: dict,
        group_key_pair: dict=None,
    ) -> dict:
        """ Decrypt a group secret.

        :return: Dict of plaintext secrets
        """
        asym = Asym()
        if group_key_pair:
            asym.set_key_pair(
                group_key_pair['public_key'], group_key_pair['private_key'])
        else:
            asym.key_pair = self.asym.key_pair
        asym.set_symmetric_key_from_encrypted(encrypted_key)
        secrets = {}
        for name, value in encrypted_secrets.items():
            secrets[name] = asym.decrypt(value)
        return secrets

    @staticmethod
    def _encrypt_secrets(
        public_key: str,
        secrets: dict
    ) -> typing.Tuple[str, dict]:
        """ Encrypt a dict of secrets using a public key

        :return: tuple (Encrypted AES key, encrypted secrets)
        """
        asym = Asym()
        key = asym.make_symmetric_key()
        encrypted_key = asym.rsa_encrypt(public_key, key, use_base64=True)

        encrypted_secrets = {}
        for name, value in secrets.items():
            encrypted_secrets[name] = asym.encrypt(value)
        return encrypted_key, encrypted_secrets
